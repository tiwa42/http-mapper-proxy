FROM amazoncorretto:19.0.1-alpine3.16
ADD ./target/HttpMapperProxy-0.0.1-SNAPSHOT.jar /opt
ENTRYPOINT ["sh", "-c", "java -jar /opt/HttpMapperProxy-0.0.1-SNAPSHOT.jar"]