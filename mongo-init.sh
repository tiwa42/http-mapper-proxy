set -e

mongosh<<EOF
db = db.getSiblingDB('httpmapper')

db.createUser({
  user: 'httpmapper',
  pwd: 'password',
  roles: [{ role: 'readWrite', db: 'httpmapper' }],
});
db.createCollection('httpEntity')
EOF
