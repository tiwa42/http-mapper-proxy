package de.tiwa.httpmapper.controller;

import java.util.List;

import org.springframework.web.bind.annotation.*;

import de.tiwa.httpmapper.model.HttpEntityPersist;
import de.tiwa.httpmapper.model.NotFoundException;
import de.tiwa.httpmapper.service.HttpEntityService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/HttpEntity")
@AllArgsConstructor
public class HttpEntityController {

    private final HttpEntityService httpEntityService;

    @GetMapping("/{id}")
    public HttpEntityPersist getHttpEntity(@PathVariable String id) {
        return httpEntityService.getHttpEntity(id).orElseThrow(NotFoundException::new);
    }

    @GetMapping("/")
    public List<HttpEntityPersist> getHttpEntitys() {
        return httpEntityService.getHttpEntities();
    }

    @PutMapping("/{id}")
    public void editHttpEntity(@PathVariable String id, @RequestBody HttpEntityPersist httpEntityPersist) {
        return;
    }
}
