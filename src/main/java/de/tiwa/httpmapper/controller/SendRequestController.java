package de.tiwa.httpmapper.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.tiwa.httpmapper.service.SendRequestService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/sendRequest")
@AllArgsConstructor
public class SendRequestController {

    private final SendRequestService sendRequestService;

    @GetMapping("/{id}")
    public void sendRequest(@PathVariable String id) {
        sendRequestService.sendRequest(id);
    }
}
