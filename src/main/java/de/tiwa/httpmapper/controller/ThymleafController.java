package de.tiwa.httpmapper.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import de.tiwa.httpmapper.model.HttpEntityPersist;
import de.tiwa.httpmapper.model.NotFoundException;
import de.tiwa.httpmapper.service.HttpEntityService;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class ThymleafController {

    private final HttpEntityService httpEntityService;

    @GetMapping("/")
    public String showHttpEntityList(Model model) {
        final List<HttpEntityPersist> httpEntities = httpEntityService.getHttpEntities();
        model.addAttribute("entities", httpEntities);
        return "index";
    }

    @PostMapping("/delete/{id}")
    public String deleteHttpEntityList(@PathVariable("id") long id, Model model) {
        final List<HttpEntityPersist> httpEntities = httpEntityService.getHttpEntities();
        model.addAttribute("entities", httpEntities);
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        final HttpEntityPersist entity = httpEntityService.getHttpEntity(Long.toString(id)).orElseThrow(NotFoundException::new);
        model.addAttribute("entity", entity);
        return "update-entity";
    }

    @PostMapping("/update/{id}")
    public String updateHttpEntity(@PathVariable("id") long id, @Valid HttpEntityPersist httpEntityPersist,
                                   BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "update-entity";
        }
        httpEntityService.updateHttpEntity(httpEntityPersist);
        return "redirect:/";
    }

}
