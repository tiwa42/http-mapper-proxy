package de.tiwa.httpmapper.persist;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import de.tiwa.httpmapper.model.HttpEntityPersist;

@Repository
public interface HttpEntityRepository extends MongoRepository<HttpEntityPersist, String> {
    Optional<HttpEntityPersist> findById(String id);
}
