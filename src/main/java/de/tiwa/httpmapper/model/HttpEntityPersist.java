package de.tiwa.httpmapper.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.http.HttpMethod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HttpEntityPersist {
    @Id
    private String id;
    private List<String> headers;
    private String body;
    private HttpMethod httpMethod;
    private String uri;
    private Date created;
    private Date lastCall;
    private int callCounter;

    public HttpEntityPersist(String id) {
        this.id = id;
        created = new Date();
        callCounter = 1;
        lastCall = new Date();
    }

    public void incrementCallCounter() {
        callCounter = callCounter + 1;
    }
}
