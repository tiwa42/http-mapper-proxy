package de.tiwa.httpmapper.service;

import java.util.List;
import java.util.Optional;

import de.tiwa.httpmapper.model.HttpEntityPersist;

public interface HttpEntityService {
    HttpEntityPersist addHttpEntity(String id);

    List<HttpEntityPersist> getHttpEntities();

    Optional<HttpEntityPersist> getHttpEntity(String id);

    Optional<HttpEntityPersist> getHttpEntityToCall(String id);

    void updateHttpEntity(HttpEntityPersist httpEntityPersist);
}
