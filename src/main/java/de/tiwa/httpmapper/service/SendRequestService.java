package de.tiwa.httpmapper.service;

public interface SendRequestService {
    void sendRequest(String id);
}
