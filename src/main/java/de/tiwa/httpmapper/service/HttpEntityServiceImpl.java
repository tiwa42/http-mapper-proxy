package de.tiwa.httpmapper.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import de.tiwa.httpmapper.model.HttpEntityPersist;
import de.tiwa.httpmapper.persist.HttpEntityRepository;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class HttpEntityServiceImpl implements HttpEntityService {
    private final HttpEntityRepository httpEntityRepository;

    @Override
    public HttpEntityPersist addHttpEntity(String id) {
        final HttpEntityPersist entity = new HttpEntityPersist(id);
        save(entity);
        return entity;
    }

    @Override
    public List<HttpEntityPersist> getHttpEntities() {
        return httpEntityRepository.findAll();
    }

    @Override
    public Optional<HttpEntityPersist> getHttpEntity(String id) {
        return httpEntityRepository.findById(id);
    }

    @Override
    public Optional<HttpEntityPersist> getHttpEntityToCall(String id) {
        final Optional<HttpEntityPersist> httpEntityOptional = getHttpEntity(id);
        if (httpEntityOptional.isPresent()) {
            final HttpEntityPersist httpEntityPersist = httpEntityOptional.get();
            httpEntityPersist.incrementCallCounter();
            save(httpEntityPersist);
        }
        return httpEntityOptional;
    }

    @Override
    public void updateHttpEntity(HttpEntityPersist httpEntityPersist) {
        save(httpEntityPersist);
    }


    private void save(HttpEntityPersist httpEntityPersist) {
        httpEntityRepository.save(httpEntityPersist);
    }
}
