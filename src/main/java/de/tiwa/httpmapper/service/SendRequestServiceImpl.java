package de.tiwa.httpmapper.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

import org.springframework.stereotype.Service;

import de.tiwa.httpmapper.model.HttpEntityPersist;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class SendRequestServiceImpl implements SendRequestService {
    private final HttpEntityService httpEntityService;

    @Override
    public void sendRequest(String id) {
        final Optional<HttpEntityPersist> httpEntity = httpEntityService.getHttpEntity(id);
        if (httpEntity.isEmpty()) {
            httpEntityService.addHttpEntity(id);
        } else {
            final HttpEntityPersist httpEntityPersist = httpEntity.get();
            HttpClient httpClient = HttpClient.newHttpClient();
            HttpRequest.Builder builder = HttpRequest.newBuilder()
                    .uri(URI.create(httpEntityPersist.getUri()));
            switch (httpEntityPersist.getHttpMethod()) {
                case GET -> builder = builder.GET();
                case PUT ->
                        builder = builder.PUT(httpEntityPersist.getBody() == null ? HttpRequest.BodyPublishers.noBody() : HttpRequest.BodyPublishers.ofString(httpEntityPersist.getBody()));
                case POST ->
                        builder = builder.POST(httpEntityPersist.getBody() == null ? HttpRequest.BodyPublishers.noBody() : HttpRequest.BodyPublishers.ofString(httpEntityPersist.getBody()));
                case DELETE -> builder = builder.DELETE();
                case HEAD -> builder = builder.HEAD();
            }
            final HttpRequest httpRequest = builder.build();
            httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString());
        }
    }
}
