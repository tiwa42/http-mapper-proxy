package de.tiwa.httpmapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class HttpMapperApplication {

    public static void main(String[] args) {
        SpringApplication.run(HttpMapperApplication.class, args);
    }
}
